package com.avg.selectimage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

import com.unity3d.player.UnityPlayer;

public class SelectImagePlugin extends UnityPlayerActivity
{
    private Activity activity;
    private final int PICK_IMAGE = 1;
    private String path;
    private Uri imageUri;
    private Bitmap selectedImage;
    private final static String FILE_NAME = "copyFile.jpg";

    public void photo()
    {
        activity = UnityPlayer.currentActivity;//Говорим, что используем активити юнити
        Intent photoPicker = new Intent(Intent.ACTION_PICK);//Создаем намерения
        photoPicker.setType("image/*");//Тип файлов
        activity.startActivityForResult(photoPicker, PICK_IMAGE);//Запуск
    }

    //Ожидание результата и его обработка
    protected void onActivityResult(int requestCode, int resultCode,  Intent imageReturnedIntent)
    {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if(resultCode == RESULT_OK)
        {
            imageUri = imageReturnedIntent.getData();
            CompressTask compressTask = new CompressTask();
            compressTask.execute();
        }
        else
        {
            String s = "FileNotFoundException";
            UnityPlayer.UnitySendMessage("ScriptManager", "GetAddress", s);
        }
    }

    class CompressTask extends AsyncTask<Void, Void, Void>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        protected Void doInBackground(Void... voids)
        {
            try
            {
                InputStream imageStream = getContentResolver().openInputStream(imageUri);
                selectedImage = BitmapFactory.decodeStream(imageStream);
                OutputStream fOut = null;
                File copyImage = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), FILE_NAME);
                try
                {
                    fOut = new FileOutputStream(copyImage);//открываем новый поток на запись
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 85, fOut);//сжимаем относительно исходного файла
                    path = copyImage.getPath();//получаем путь для передачи в Unity
                    fOut.close();//закрываем поток
                }
                catch (FileNotFoundException e)
                {
                    e.printStackTrace();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);
            UnityPlayer.UnitySendMessage("ScriptManager", "GetAddress", path);
        }
    }
}

