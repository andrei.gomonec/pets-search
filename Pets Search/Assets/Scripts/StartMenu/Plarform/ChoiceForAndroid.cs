#if UNITY_ANDROID
using UnityEngine;

public class ChoiceForAndroid : MonoBehaviour
{
    private AndroidJavaObject _pluginActivity;

    public void SelectedPhoto()
    {
        _pluginActivity = new AndroidJavaObject("com.avg.selectimage.SelectImagePlugin");
        AndroidJavaClass ajc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        _pluginActivity.Call("photo");
    }

    public void GetAddress(string path)
    {
        if (path == "FileNotFoundException")
        {
            DisplaySet displaySet = GetComponent<DisplaySet>();
            displaySet.OffCanvas();
        }
        else
        {
            SettingFile settingFile = GetComponent<SettingFile>();
            settingFile.GetPath(path);
            Data.savePath = path;
        }
    }
}
#endif
