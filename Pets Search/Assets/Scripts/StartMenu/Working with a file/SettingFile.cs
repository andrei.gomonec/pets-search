using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SettingFile : MonoBehaviour
{
    private string _pathToFile;
    private float _widthPhoto;
    private float _heightPhoto;
    private const float RESOLUTION_WIGHT = 1920; //����������� �� canvas �� ���������
    private const float MAX_HEIGHT = 2700;
    private DisplaySet _displaySet;
    [SerializeField] private RawImage _rawImage;

    public void GetPath(string path)
    {
        _displaySet = GetComponent<DisplaySet>();
        _pathToFile = path;
        StartCoroutine(DownloadImage("file://" + _pathToFile));
    }

    private IEnumerator DownloadImage(string MediaUrl)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.Log(request.error);
            _displaySet.OffCanvas();
        }
        else
        {
            UpdatingSize(((DownloadHandlerTexture)request.downloadHandler).texture);
        }
    }

    private void UpdatingSize(Texture2D photo)
    {
        _widthPhoto = photo.width;
        _heightPhoto = photo.height;
        SetSizeTexture();
        Load();
        Setting(photo);
    }

    private void SetSizeTexture()
    {
        if (_widthPhoto > Screen.width) //���� ������ ���� ������ ��� �����, �� ��������� �� 
        {
            float delta = _widthPhoto / RESOLUTION_WIGHT;
            _widthPhoto = _widthPhoto / delta;
            _heightPhoto = _heightPhoto / delta;
        }

        if (_widthPhoto <= Screen.width) //���� ������ ���� ������ ��� �����, �� ����������� ��
        {
            float delta = RESOLUTION_WIGHT / _widthPhoto;
            _widthPhoto = _widthPhoto * delta;
            _heightPhoto = _heightPhoto * delta;
        }

        if(_heightPhoto >= MAX_HEIGHT)
        {
            float delta = _heightPhoto / MAX_HEIGHT;
            _widthPhoto = _widthPhoto / delta;
            _heightPhoto = _heightPhoto / delta;
        }
    }

    private void Setting(Texture2D photo)
    {
        SetSizeRawImage(_rawImage.rectTransform, new Vector2(_widthPhoto, _heightPhoto));
        _rawImage.gameObject.SetActive(true);
        _rawImage.texture = photo;
    }

    private void SetSizeRawImage(RectTransform trans, Vector2 newSize)
    {
        Vector2 oldSize = trans.rect.size;
        Vector2 deltaSize = newSize - oldSize;
        trans.offsetMin = trans.offsetMin - new Vector2(deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y);
        trans.offsetMax = trans.offsetMax + new Vector2(deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y));
    }

    private void Load()
    {
        _displaySet.OnNewFrame();
    }
}
