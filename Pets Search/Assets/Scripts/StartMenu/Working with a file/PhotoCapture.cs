using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PhotoCapture : MonoBehaviour
{
    [SerializeField] private Camera _cameraCapture;

    public void MakeScreenshot()
    {
        
        Texture2D texture = new Texture2D(800, 800);

        RenderTexture renderTexture = RenderTexture.GetTemporary(800, 800);
        this._cameraCapture.targetTexture = renderTexture;
        this._cameraCapture.Render();

        RenderTexture.active = renderTexture;

        Rect rect = new Rect(0, 0, 800, 800);
        texture.ReadPixels(rect, 0, 0);
        texture.Apply();
        byte[] data = texture.EncodeToJPG();
#if UNITY_ANDROID
        File.WriteAllBytes(Data.savePath, data);
#endif
    }


}
