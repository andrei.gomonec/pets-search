using UnityEngine;

public class Scaling : MonoBehaviour
{
    private const float ZOOM_FACTOR = 0.0025f;
    private const float MIN_SCALE = 0.65f;
    private const float MAX_SCALE = 4.5f;
    private float _wheelTime = 0.25f;

    private Touch _touchZero;
    private Touch _touchOne;
    private bool _isScaling = false;

    private void Update()
    {
        /*if (Input.GetMouseButtonDown(0))
        {
            Vector2 mouse = Input.mousePosition;
            this.transform.position = new Vector3(mouse.x, mouse.y, this.transform.position.z);
            
        }
        
        if(Input.touchCount == 1 && _isScaling == false)
        {

        }*/

        if (Input.touchCount == 2)
        {
            _touchZero = Input.GetTouch(0);
            _touchOne = Input.GetTouch(1);
            _isScaling = true;

            Vector2 touchZeroLastPosition = _touchZero.position - _touchZero.deltaPosition;
            Vector2 touchOneLastPosition = _touchOne.position - _touchOne.deltaPosition;

            float distTouch = (touchZeroLastPosition - touchOneLastPosition).magnitude;
            float currentDistTouch = (_touchZero.position - _touchOne.position).magnitude;

            float difference = currentDistTouch - distTouch;

            Zoom(difference * ZOOM_FACTOR);

            if(_touchZero.phase == TouchPhase.Ended || _touchOne.phase == TouchPhase.Ended)
            {
                _isScaling = false;
                ReturnScale();
            }
        }


#if UNITY_EDITOR_WIN
        
        float mouseWheel = Input.GetAxis("Mouse ScrollWheel");
        if(mouseWheel != 0f)
        {
            _wheelTime = 0.25f;
            Zoom(mouseWheel);
        }

        _wheelTime -= Time.deltaTime;

        if (_wheelTime <= 0)
        {
            ReturnScale();
        }
#endif
    }

    private void Zoom(float increment)
    {
        this.transform.localScale = new Vector3
            (Mathf.Clamp(this.transform.localScale.x + increment, MIN_SCALE, MAX_SCALE),
             Mathf.Clamp(this.transform.localScale.y + increment, MIN_SCALE, MAX_SCALE),
             Mathf.Clamp(this.transform.localScale.z + increment, MIN_SCALE, MAX_SCALE));
    }

    private void ReturnScale()
    {
        if(this.transform.localScale.x < 0.7f)
        {
            this.transform.localScale = Vector3.one;
        }
    }
}
