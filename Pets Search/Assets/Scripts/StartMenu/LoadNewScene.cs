using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNewScene : MonoBehaviour
{
    private string _pathToFile;

    public void LoadScene()
    {
        _pathToFile = Data.savePath;
        if (_pathToFile != null)
        {
            SceneManager.LoadScene(1);
        }
    }
}
