using UnityEngine;

public class ButtonClick : MonoBehaviour
{
    public void SelectedPhotoButton()
    {
        DisplaySet displaySet = GetComponent<DisplaySet>();
        displaySet.OnCanvasExpectation();
#if UNITY_ANDROID
        ChoiceForAndroid android = GetComponent<ChoiceForAndroid>();
        android.SelectedPhoto();
#endif
#if UNITY_EDITOR
        ChoiceForWindows windows = GetComponent<ChoiceForWindows>();
        windows.SelectedPhoto(); 
#endif
    }
}
