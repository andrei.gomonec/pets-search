using UnityEngine;

public class DisplaySet : MonoBehaviour
{
    [Header("������ ��������")]
    [SerializeField] private GameObject _expectation;
    [Header("������ �����")]
    [SerializeField] private GameObject _searchMenu;
    [Header("������ ����")]
    [SerializeField] private GameObject _photo;
    [Header("������ �����")]
    [SerializeField] private GameObject _startMenu;

    public void OffCanvas()
    {
        _expectation.SetActive(false);
    }

    public void OnCanvasExpectation()
    {
        _expectation.SetActive(true);
    }

    public void OnNewFrame()
    {
        OffCanvas();
        _startMenu.SetActive(false);
        _photo.SetActive(true);
        _searchMenu.SetActive(true);
    }

    public void BackStartMenu()
    {
        _startMenu.SetActive(true);
        _photo.SetActive(false);
        _searchMenu.SetActive(false);
    }
}
