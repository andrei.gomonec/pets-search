using UnityEngine;
using UnityEngine.UI;

public class GetPhotoFile : MonoBehaviour
{
    [SerializeField] private RawImage rawImage;
    private string _pathToFile;

    private void Start()
    {
        GetPhoto();
    }
    private void GetPhoto()
    {
        _pathToFile = Data.savePath;
        WWW www = new WWW("file://" +_pathToFile);
        Texture2D texture = www.texture;
        rawImage.texture = texture;
        //rawImage.SetNativeSize();*/
    }
}
