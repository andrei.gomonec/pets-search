using UnityEngine.SceneManagement;
using UnityEngine;

public class CloseScene : MonoBehaviour
{
    public void Close()
    {
        SceneManager.LoadScene(0);
    }
}
